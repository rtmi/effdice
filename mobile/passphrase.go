package main

import (
	"bufio"
	"flag"
	"log"
	"os"
	"path/filepath"
	"strings"

	"golang.org/x/mobile/asset"
)

var eff = flag.String("eff", "eff_large_wordlist.txt", "EFF phrase list file")
var logfile = flag.String("log", "", "Output log file")

func NewPassphrase() string {

	flag.Parse()
	if *logfile != "" {
		dir, err := os.Getwd()
		if err != nil {
			panic(err)
		}
		path := filepath.Join(dir, *logfile)
		w, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		if err != nil {
			panic(err)
		}
		defer w.Close()
		log.SetOutput(w)
	}

	m := loadEffDictionary(*eff)
	p := make([]string, 6)
	for i := 0; i < 6; i++ {

		key, err := Rolldice()
		if err != nil {
			log.Fatal(err)
			panic(err)
		}

		if val, ok := m[key]; ok {
			p[i] = val
			log.Printf("roll %d: %v (%v)\n", i, key, val)
		}
	}

	log.Printf("passphrase: %v\n", strings.Join(p, " "))
	return strings.Join(p, " ")
}

func loadEffDictionary(filename string) map[string]string {

	m := make(map[string]string)

	file, err := asset.Open(filename)
	if err != nil {
		log.Println("error opening wordlist asset:", err)
		return m
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		pair := strings.Split(scanner.Text(), "\t")
		key := strings.TrimSpace(pair[0])
		val := strings.TrimSpace(pair[1])
		m[key] = val
	}
	if err := scanner.Err(); err != nil {
		log.Println("reading wordlist:", err)
	}

	return m
}
