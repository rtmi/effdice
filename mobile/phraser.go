// +build darwin linux

package main

import (
	"image"
	"image/color"
	"log"
	"strings"

	"github.com/golang/freetype/truetype"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/exp/gl/glutil"
	"golang.org/x/mobile/geom"
	"golang.org/x/mobile/gl"
)

type Phraser struct {
	body       string
	font       *truetype.Font
	bgfont     *truetype.Font
	cycle      uint8
}

func NewPhraser() *Phraser {
	var g Phraser
	g.reset()
	return &g
}

func (g *Phraser) reset() {
	var err error
	g.font, err = LoadBodyFont()
	if err != nil {
		log.Fatalf("error parsing font: %v", err)
	}
	g.bgfont, err = LoadBackgroundFont()
	if err != nil {
		log.Fatalf("error parsing font: %v", err)
	}
}

func (g *Phraser) Touch(down bool) {

}

func (g *Phraser) BodyText(txt string) {
	g.body = txt
}

func (g *Phraser) Render(sz size.Event, glctx gl.Context, images *glutil.Images) {

	headerHeightPx := zoomHdrHt(sz)
	footerHeightPx := headerHeightPx
	fontsz := zoomFont(sz, 24)

	header := &TextSprite{
		text:            "Passphrase",
		font:            g.font,
		widthPx:         sz.WidthPx,
		heightPx:        headerHeightPx,
		textColor:       image.Black,
		backgroundColor: image.NewUniform(color.RGBA{0xF2, 0x02, 0x02, 0xFF}),
		fontSize:        fontsz,
		xPt:             0,
		yPt:             0,
		align:           Left,
	}
	header.Render(sz)

	g.orientBody(sz, headerHeightPx, footerHeightPx, fontsz)

	g.cycle += 1
	if g.cycle > 254 {
		g.cycle = 0
	}
	footer := &TextSprite{
		text:            "EFF",
		font:            g.bgfont,
		widthPx:         sz.WidthPx,
		heightPx:        footerHeightPx,
		textColor:       image.NewUniform(color.RGBA{g.cycle, 0x00, 0x00, 0xFF}),
		backgroundColor: image.NewUniform(color.RGBA{0xF2, 0x02, 0x02, 0xFF}),
		fontSize:        zoomFont(sz, 48),
		xPt:             0,
		yPt:             PxToPt(sz, sz.HeightPx-footerHeightPx),
		align:           Right,
	}
	footer.Render(sz)
}

// PxToPt convert a size from pixels to points (based on screen PixelsPerPt)
func PxToPt(sz size.Event, sizePx int) geom.Pt {
	return geom.Pt(float32(sizePx) / sz.PixelsPerPt)
}

// zoomHdrHt calculates 1/6th ratio of screen as hdr/ftr height
func zoomHdrHt(sz size.Event) int {
	scale := sz.HeightPx / 6
	return int(scale)
}

// zoomFont calculates base/688 ratio of screen as font size
func zoomFont(sz size.Event, base int) float64 {
	scale := sz.HeightPx / 688
	return float64(scale * base)
}

// orientBody makes landscape single line and portrait 6 lines
func (g *Phraser) orientBody(sz size.Event, hht int, fht int, fontsz float64) {
	bodyht := sz.HeightPx - hht - fht

	if sz.Orientation == size.OrientationLandscape {

		g.drawBodyRow(sz, hht, bodyht, g.body, fontsz)

	} else {
		words := strings.Split(g.body, " ")
		lineHeight := bodyht / 6

		for i := 0; i < 5; i++ {
			g.drawBodyRow(sz, hht+(i*lineHeight),
				lineHeight, words[i], fontsz)
		}
		// Finish with remaining body height
		remaininght := bodyht - (5 * lineHeight)
		g.drawBodyRow(sz, hht+(5*lineHeight),
			remaininght, words[5], fontsz)
	}
}

func (g *Phraser) drawBodyRow(sz size.Event, startht int, rowht int, word string, fontsz float64) {

	row := &TextSprite{
		text:            word,
		font:            g.font,
		widthPx:         sz.WidthPx,
		heightPx:        rowht,
		textColor:       image.White,
		backgroundColor: image.NewUniform(color.RGBA{0x00, 0x00, 0x00, 0xFF}),
		fontSize:        fontsz,
		xPt:             0,
		yPt:             PxToPt(sz, startht),
	}
	row.Render(sz)
}
