# android-effdice
This is a Docker image to make a
 [EFF Dice](https://www.eff.org/dice) Android app 
 in Golang with the Gomobile toolchain. The container
 helps save time by having the environment prep'd and
 ready with tools to build the APK.

![Image of Android app](https://patterns.github.io/tfx/images/android-effdice.png)

## Quickstart
1. Install [Docker](https://docker.com/)
2. Git clone the [src](https://github.com/patterns/effdice/)
3. From the cloned directory, build and run


```console
$ docker build -t android mobile
$ docker run -it --rm -v $(pwd):/go/app/effdice android
# cp app.apk /go/app/effdice/effdice.apk
# exit
$ adb install effdice.apk
```

## Credits
The original Dockerfile started from 
 [Bitrise](https://github.com/bitrise-docker/android-ndk)

VTC Letterer Pro is by [Vigilante Typeface Corp](https://www.fontsquirrel.com/fonts/vtc-letterer-pro)

Plexifont is by [Blue Vinyl Fonts](https://www.fontsquirrel.com/fonts/plexifont-bv)

Truetype font (TTF) rendering is from
 [Antoine Richard](https://github.com/antoine-richard/gomobile-text/)

