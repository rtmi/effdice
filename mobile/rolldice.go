package main

import (
	"crypto/rand"
	"log"
	"math/big"
)

// Rolldice returns a 5 digit string representing 5 dice outcomes.
func Rolldice() (string, error) {

	outcomes := int64(5)
	diefaces := int64(5)
	singleroll := make([]*big.Int, outcomes)
	max := big.NewInt(diefaces)

	for i := int64(0); i < outcomes; i++ {

		val, err := rand.Int(rand.Reader, max)

		if err != nil {
			log.Println("ERROR from Random Integer (0-6):", err)
			return "", err
		}

		singleroll[i] = val.Add(val, big.NewInt(1))
	}

	return outcomesToString(singleroll), nil
}

func outcomesToString(outcomes []*big.Int) string {

	// This is one way to append/concat shown on SO.

	sz := len(outcomes)
	bs := make([]byte, sz)
	bl := 0

	for _, v := range outcomes {
		bl += copy(bs[bl:], v.String())
	}

	return string(bs)
}
