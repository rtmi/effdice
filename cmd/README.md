
# What is this?

This is a Docker image for EFF Dice. It follows the steps
described on the EFF page. It's implemented in Golang, but
Docker lets us run it without installing Go tools. Docker
also allows execution on Linux/Win/MacOS

> [www.eff.org/dice](https://www.eff.org/dice)


# How to use this image

## With the source code

1. Install [Docker](https://docker.com/)
2. Git clone the [src](https://github.com/patterns/effdice/)
3. From the cloned directory, build and run


You can build and run the Docker image:

```console
$ docker build -t effdice cmd
$ docker run -it --rm effdice
```


## With the pre-built image

1. Install [Docker](https://docker.com/)
2. Run

```console
$ docker run -it --rm patterns/effdice
```


