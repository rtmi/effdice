package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
)

var eff = flag.String("eff", "eff_large_wordlist.txt", "EFF phrase list file")
var logfile = flag.String("log", "", "Output log file")

func main() {

	flag.Parse()
	if *logfile != "" {
		dir, err := os.Getwd()
		if err != nil {
			panic(err)
		}
		path := filepath.Join(dir, *logfile)
		w, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		if err != nil {
			panic(err)
		}
		defer w.Close()
		log.SetOutput(w)
	}

	m := loadEffDictionary(*eff)
	p := make([]string, 6)
	for i := 0; i < 6; i++ {

		key, err := Rolldice()
		if err != nil {
			log.Fatal(err)
			return
		}

		if val, ok := m[key]; ok {
			p[i] = val
			log.Printf("roll %d: %v (%v)\n", i, key, val)
		}
	}

	log.Printf("passphrase: %v\n", strings.Join(p, " "))
	fmt.Printf("passphrase: %v\n", strings.Join(p, " "))
}

func loadEffDictionary(filename string) map[string]string {

	m := make(map[string]string)

	// Restricting to working dir, for now.
	dir, err := os.Getwd()
	if err != nil {
		log.Println("ERROR from Working Dir:", err)
		return m
	}

	p := filepath.Join(dir, filename)
	if _, err := os.Stat(p); os.IsNotExist(err) {
		log.Println("file does not exist")
		return m
	}

	f, err := os.OpenFile(p, os.O_RDONLY, 0755)
	if err != nil {
		// Ignoring, for now not fatal.
		log.Println("ERROR from EFF Words File:", err)
		return m
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		pair := strings.Split(scanner.Text(), "\t")
		key := strings.TrimSpace(pair[0])
		val := strings.TrimSpace(pair[1])
		m[key] = val
	}
	if err := scanner.Err(); err != nil {
		log.Println("reading standard input:", err)
	}

	return m
}
