package main

import (
	"fmt"
	"regexp"
	"testing"
)

func TestNonemptyDiceroll(t *testing.T) {
	v, _ := Rolldice()
	if v == "" {
		t.Error(`Dice roll resulted in empty value`)
	}
}

func TestFiveDigitsOutcome(t *testing.T) {
	v, _ := Rolldice()
	if len(v) != 5 {
		t.Error(`Dice roll resulted in unexpected size`)
	}
}

func TestDigitBounds(t *testing.T) {
	var p = regexp.MustCompile(`[1-5]{5}`) //todo disambigu-ify regex
	v, _ := Rolldice()
	if !p.MatchString(v) {
		t.Error(`Dice roll resulted in unexpected value`)
	}
}

func TestRandom(t *testing.T) {
		m := make(map[string]int, 1000)
		for i := 0; i < 1000; i++ {
			v := sequence()
			//debug
			////fmt.Printf("matching: %v\n", v)
			//debug

			if d, ok := m[v]; ok {
				if d > 0 {
					t.Errorf("Dice roll sample is not random: %v x %d",
						v, d)
					m[v] = d + 1
				}
			} else {
				m[v] = 1
			}
		}
}

func sequence() string {
	v1, _ := Rolldice()
	v2, _ := Rolldice()
	v3, _ := Rolldice()
	v4, _ := Rolldice()
	v5, _ := Rolldice()
	return fmt.Sprintf("%v%v%v%v%v", v1, v2, v3, v4, v5)
}
