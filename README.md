# effdice

This is a program for creating passphrases.
 It follows the steps described on the EFF Dice page.

> [www.eff.org/dice](https://www.eff.org/dice)

There's two version:

- Command line (/cmd)
- Mobile app (/mobile)

